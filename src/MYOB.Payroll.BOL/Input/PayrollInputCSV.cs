﻿using System;
using System.Linq;
using System.Collections.Generic;
using MYOB.Payroll.BOL.Models;
using System.IO;

namespace MYOB.Payroll.BOL.Input
{
    /// <summary>
    /// Implements the CSV input payroll processor
    /// </summary>
    public class PayrollInputCSV : IPayrollInput
    {
        //Define the CSV columns
        private enum FileLayout : int
        {
            FirstName = 0,
            LastName = 1,
            AnnualSalary = 2,
            SuperRate = 3,
            PaymentPeriod = 4
        }

        private const char SEPERATOR_CSV = ',';
        private const char CONST_PERCENT = '%';

        /// <summary>
        /// Loads payroll input records from the file specified by the <paramref name="inputConfig"/> parameter
        /// </summary>
        /// <param name="inputConfig">Filename to load the payroll records from</param>
        /// <returns>IList of PayrollInput records</returns>
        public IList<PayrollInput> ProcessInput(string inputConfig)
        {
            if (string.IsNullOrEmpty(inputConfig))
                throw new ArgumentNullException("inputConfig", SystemStrings.NULL_INPUTS);

            if (!File.Exists(inputConfig))
                throw new FileNotFoundException(SystemStrings.FILE_NOT_FOUND, inputConfig);

            return loadRecords(inputConfig);
        }

        private IList<PayrollInput> loadRecords(string inputConfig)
        {
            return File.ReadAllLines(inputConfig)
                .Skip(1)                            //First row contains header
                .Where(line => line.Length > 1)     //Last row could be empty
                .Select(TransformToPayrollInput)
                .ToList<PayrollInput>(); 
        }

        private PayrollInput TransformToPayrollInput(string line)
        {
            string[] data = line.Split(SEPERATOR_CSV);
            return new PayrollInput()
            {
                FirstName = data[(int)FileLayout.FirstName],
                LastName = data[(int)FileLayout.LastName],
                AnnualSalary = int.Parse(data[(int)FileLayout.AnnualSalary]),
                SuperRate = decimal.Parse(data[(int)FileLayout.SuperRate].TrimEnd(CONST_PERCENT)) / 100,
                PaymentPeriod = data[(int)FileLayout.PaymentPeriod]
            };
        }
    }
}
