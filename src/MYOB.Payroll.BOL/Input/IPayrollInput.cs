﻿using MYOB.Payroll.BOL.Models;
using System.Collections.Generic;

namespace MYOB.Payroll.BOL.Input
{
    /// <summary>
    /// Defines interface for payroll input records.
    /// Implementations of this interface must implement the ProcessInput method
    /// </summary>
    public interface IPayrollInput
    {
        /// <summary>
        /// Process the input parameter and produce a IList of payroll input records
        /// </summary>
        /// <param name="inputParameters">Depending on the implementation, inputParameter can either be a
        /// path, or connection string, or ...
        /// </param>
        /// <returns>IList of PayrollInput records</returns>
        IList<PayrollInput> ProcessInput(string inputConfig);
    }
}
