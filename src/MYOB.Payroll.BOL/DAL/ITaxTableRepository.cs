﻿using MYOB.Payroll.BOL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MYOB.Payroll.BOL.DAL
{
    /// <summary>
    /// Repository to supply the application with TaxTable information
    /// </summary>
    public interface ITaxTableRepository 
    {
        /// <summary>
        /// Will return a list of tax tables
        /// </summary>
        /// <returns></returns>
        IEnumerable<TaxTable> GetTaxTables();

        /// <summary>
        /// Will return a TaxTable based on the Salary passed in
        /// </summary>
        /// <param name="annualSalary"></param>
        /// <returns></returns>
        TaxTable GetTaxTableBySalary(int annualSalary);
    }
}
