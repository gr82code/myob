﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MYOB.Payroll.BOL.Models;
using System.IO;

namespace MYOB.Payroll.BOL.DAL
{
    public class TaxTableRepositoryCSV : ITaxTableRepository
    {
        public IList<TaxTable> _taxTables;
        public const char SEPERATOR_CSV = ',';

        public TaxTable GetTaxTableBySalary(int annualSalary)
        {
            return GetTaxTables()
                .Where(i => annualSalary > i.LowAmount && annualSalary < i.HighAmount)
                .Take(1)
                .Select(i => i)
                .FirstOrDefault();
        }

        public IEnumerable<TaxTable> GetTaxTables()
        {
            if (_taxTables == null)
            {
                if (!File.Exists(SystemStrings.TAX_TABLE_PATH))
                    throw new FileNotFoundException(SystemStrings.TAX_TABLE_NOT_FOUND, SystemStrings.TAX_TABLE_PATH);

                _taxTables = File.ReadAllLines(SystemStrings.TAX_TABLE_PATH)
                    .Skip(1)                        //First row contains header
                    .Where(line => line.Length > 1) //Last row could be empty
                    .Select(t =>
                    {
                        string[] data = t.Split(SEPERATOR_CSV);
                        return new TaxTable()
                        {
                            LowAmount = int.Parse(data[(int)TaxTable.FileLayout.LowAmount]),
                            HighAmount = int.Parse(data[(int)TaxTable.FileLayout.HighAmount]),
                            BaseAmount = int.Parse(data[(int)TaxTable.FileLayout.BaseAmount]),
                            FreeAmount = int.Parse(data[(int)TaxTable.FileLayout.FreeAmount]),
                            TaxRate = decimal.Parse(data[(int)TaxTable.FileLayout.TaxRate])
                        };
                    })
                    .ToList<TaxTable>();
            }

            return _taxTables;
        }
    }
}
