﻿using System;

namespace MYOB.Payroll.BOL.Models
{
    /// <summary>
    /// Defines the layout for payroll input records
    /// </summary>
    public class PayrollInput
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int AnnualSalary { get; set; }
        public decimal SuperRate { get; set; }
        public string PaymentPeriod { get; set; }
    }
}
