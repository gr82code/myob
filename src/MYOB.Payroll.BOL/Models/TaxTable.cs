﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MYOB.Payroll.BOL.Models
{
    /// <summary>
    /// Defines the layout for TaxTable record
    /// </summary>
    public class TaxTable
    {
        //Define tax table columns
        public enum FileLayout : int
        {
            LowAmount = 0,
            HighAmount = 1,
            BaseAmount = 2,
            FreeAmount = 3,
            TaxRate = 4
        }

        public int LowAmount { get; set; }
        public int HighAmount { get; set; }
        public int BaseAmount { get; set; }
        public int FreeAmount { get; set; }
        public decimal TaxRate { get; set; }
    }
}
