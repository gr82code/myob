﻿using System;
/// <summary>
/// Defines the layout for payroll output records
/// </summary>
namespace MYOB.Payroll.BOL.Models
{
    /// <summary>
    /// Defines the layout for Payroll Output Records
    /// </summary>
    public class PayrollOutput
    {
        public string Name { get; set; }
        public string PaymentPeriod { get; set; }
        public int GrossIncome { get; set; }
        public int IncomeTax { get; set; }
        public int NetIncome { get; set; }
        public int Super { get; set; }
    }
}
