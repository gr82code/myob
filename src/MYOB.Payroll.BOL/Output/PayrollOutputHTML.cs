﻿using System;
using System.Collections.Generic;
using MYOB.Payroll.BOL.Models;
using System.Xml.Linq;
using System.Linq;

namespace MYOB.Payroll.BOL.Output
{
    /// <summary>
    /// Produces a xml file of the payroll records
    /// </summary>
    public class PayrollOutputHTML : PayrollOutputFile
    {
        public override bool ProcessOutput(IList<PayrollOutput> payrollOutput, string outputConfig)
        {
            base.ProcessOutput(payrollOutput, outputConfig);

            XDocument xdoc = new XDocument(
                new XElement("table",
                    new XElement("tr",
                        new XElement("th", "name"),
                        new XElement("th", "payPeriod"),
                        new XElement("th", "grossIncome"),
                        new XElement("th", "incomeTax"),
                        new XElement("th", "netIncome"),
                        new XElement("th", "super")),
                    from employee in payrollOutput
                    select new XElement("tr",
                         new XElement("td", employee.Name),
                         new XElement("td", employee.PaymentPeriod),
                         new XElement("td", employee.GrossIncome),
                         new XElement("td", employee.IncomeTax),
                         new XElement("td", employee.NetIncome),
                         new XElement("td", employee.Super))
             ));

            xdoc.Save(outputConfig);

            return true;
        }
    }
}
