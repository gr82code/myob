﻿using MYOB.Payroll.BOL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MYOB.Payroll.BOL.Output
{
    /// <summary>
    /// Abstract class to implement common functionality of File based PayrollOutput Implementations
    /// In a typical implementation, the outputConfig parameters wil be the name of the output file
    /// </summary>
    public abstract class PayrollOutputFile : IPayrollOutput
    {
        public virtual bool ProcessOutput(IList<PayrollOutput> payrollOutput, string outputConfig)
        {
            if (null == payrollOutput)
                throw new ArgumentNullException("payrollOutput", SystemStrings.NULL_SOURCE);

            if (File.Exists(outputConfig))
                File.Delete(outputConfig);

            return true;
        }
    }
}
