﻿using System.Collections.Generic;
using MYOB.Payroll.BOL.Models;
using System.Xml.Linq;
using System.Linq;

namespace MYOB.Payroll.BOL.Output
{
    /// <summary>
    /// Produces a xml file of the payroll records
    /// </summary>
    public class PayrollOutputXML : PayrollOutputFile
    {
        public override bool ProcessOutput(IList<PayrollOutput> payrollOutput, string outputConfig)
        {
            base.ProcessOutput(payrollOutput, outputConfig);

            XDocument xdoc = new XDocument(
                new XElement("payroll",
                    from employee in payrollOutput
                    select new XElement("employee",
                         new XElement("name", employee.Name),
                         new XElement("payPeriod", employee.PaymentPeriod),
                         new XElement("grossIncome", employee.GrossIncome),
                         new XElement("incomeTax", employee.IncomeTax),
                         new XElement("netIncome", employee.NetIncome),
                         new XElement("super", employee.Super))
             ));

            xdoc.Save(outputConfig);

            return true;
        }
    }
}
