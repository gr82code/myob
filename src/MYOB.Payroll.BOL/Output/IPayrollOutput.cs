﻿using MYOB.Payroll.BOL.Models;
using System.Collections.Generic;

namespace MYOB.Payroll.BOL.Output
{
    /// <summary>
    /// Defines interface for PayrollOutput records
    /// Implementations of this interface must implement the ProcessOutput method
    /// </summary>
    public interface IPayrollOutput
    {
        /// <summary>
        /// Processes the <paramref name="payrollOutput"/> parameter using the outputParameter
        /// </summary>
        /// <param name="payrollOutput">IList of PayrollOutput</param>
        /// <param name="outputConfig">Depending on the implementation, <paramref name="outputConfig"/> can either be a path, 
        /// or connection string, or ...</param>
        /// <returns></returns>
        bool ProcessOutput(IList<PayrollOutput> payrollOutput, string outputConfig);
    }
}
