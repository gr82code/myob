﻿using System;
using System.Collections.Generic;
using MYOB.Payroll.BOL.Models;
using Newtonsoft.Json;
using System.IO;

namespace MYOB.Payroll.BOL.Output
{
    /// <summary>
    /// Produces a json file of the payroll records
    /// </summary>
    public class PayrollOutputJSON : PayrollOutputFile
    {
        public override bool ProcessOutput(IList<PayrollOutput> payrollOutput, string outputConfig)
        {
            base.ProcessOutput(payrollOutput, outputConfig);
            var json = JsonConvert.SerializeObject(payrollOutput);
            File.WriteAllText(outputConfig, json);

            return true;
        }
    }
}
