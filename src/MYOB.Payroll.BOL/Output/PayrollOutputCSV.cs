﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MYOB.Payroll.BOL.Models;
using System.IO;

namespace MYOB.Payroll.BOL.Output
{
    /// <summary>
    /// Produces a csv file of the payroll records
    /// </summary>
    public class PayrollOutputCSV : PayrollOutputFile
    {
        
        private const string CSV_HEADER = "name,pay period,gross income,income tax,net income,super";

        public override bool ProcessOutput(IList<PayrollOutput> payrollOutput, string outputConfig)
        {
            if (!base.ProcessOutput(payrollOutput, outputConfig))
                return false;

            var stream = File.OpenWrite(outputConfig);
            using (var writer = new StreamWriter(stream))
            {
                writer.WriteLine(CSV_HEADER);
                var result = payrollOutput
                .Select(x => $"{x.Name},{x.PaymentPeriod},{x.GrossIncome},{x.IncomeTax},{x.NetIncome},{x.Super}");
                foreach (var line in result)
                    writer.WriteLine(line);
            }

            return true;
        }
    }
}
