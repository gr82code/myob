﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MYOB.Payroll.BOL.Models;
using System.IO;

namespace MYOB.Payroll.BOL
{
    /// <summary>
    /// Common class for payroll opperations. Implemented using the Singleton pattern
    /// </summary>
    public class Common
    {
        private static Common _instance;
        
        private Common() { }

        public static Common Instance
        {
            get
            {
                _instance = _instance ?? new Common();
                return _instance;
            }
        }
    }
}
