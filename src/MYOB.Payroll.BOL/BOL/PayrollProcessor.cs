﻿using log4net;
using MYOB.Payroll.BOL.DAL;
using MYOB.Payroll.BOL.Input;
using MYOB.Payroll.BOL.Models;
using MYOB.Payroll.BOL.Output;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MYOB.Payroll.BOL
{
    /// <summary>
    /// Main payroll processor class.
    /// </summary>
    public class PayrollProcessor
    {
        ITaxTableRepository _taxTableRepository;
        IPayrollInput _payrollInput;
        IPayrollOutput _payrollOutput;
        ILog _log;

        /// <summary>
        /// PayrollProcessor constructor.
        /// Throws ApplicationException
        /// </summary>
        /// <param name="taxTableRepository">The TableRepository implementation to use</param>
        /// <param name="payrollInput">The PayrollInput implementation to use</param>
        /// <param name="payrollOutput">The PayrollOutput implementation to use</param>
        /// <param name="log">The Log provider to use</param>
        public PayrollProcessor(
            ITaxTableRepository taxTableRepository,
            IPayrollInput payrollInput, 
            IPayrollOutput payrollOutput, 
            ILog log)
        {
            try
            {
                _log = log;

                if (null== taxTableRepository || null == payrollInput || null == payrollOutput || null == log)
                    throw new ArgumentNullException(
                        "taxTableRepository,payrollInput,payrollOutput,log", 
                        SystemStrings.NULL_INPUTS);

                _taxTableRepository = taxTableRepository;
                _payrollInput = payrollInput;
                _payrollOutput = payrollOutput;
            }
            catch (Exception ex)
            {
                if (null != _log)
                    _log.Error(SystemStrings.PROCESSING_ERROR, ex);

                throw new ApplicationException(SystemStrings.PROCESSING_ERROR, ex);
            }
        }

        /// <summary>
        /// Loads payroll records from the PayrollInput implementation, using the inputConfig.
        /// Outputs payroll records to the PayrollOutput implementation, using the outputConfig.
        /// Throws ApplicationException
        /// </summary>
        /// <param name="inputConfig">Config required by the PayrollInput implementation</param>
        /// <param name="outputConfig">Config required by the PayrollOutput implementation</param>
        /// <returns>True if the records were processed successfully</returns>
        public bool ProcessInput(string inputConfig, string outputConfig)
        {
            try
            {
                IList<PayrollInput> inputData = _payrollInput.ProcessInput(inputConfig);
                IList<PayrollOutput> outputData = inputData.Select(GetPayrollOutput).ToList();
                return _payrollOutput.ProcessOutput(outputData, outputConfig);
            }
            catch (Exception ex)
            {
                _log.Error(SystemStrings.PROCESSING_ERROR, ex);
                throw new ApplicationException(SystemStrings.PROCESSING_ERROR, ex);
            }
        }

        public PayrollOutput GetPayrollOutput(PayrollInput payrollInput)
        {
            int grossIncome = CalculateGrossIncome(payrollInput);
            int incomeTax = CalculateIncomeTax(payrollInput);
            int netIncome = grossIncome - incomeTax;
            int super = (int)Math.Round(grossIncome * payrollInput.SuperRate, 0);

            //Construct a new PayrolOutput record for the input
            return new PayrollOutput()
            {
                Name = $"{payrollInput.FirstName} {payrollInput.LastName}",
                PaymentPeriod = payrollInput.PaymentPeriod,
                GrossIncome = grossIncome,
                IncomeTax = incomeTax,
                NetIncome = netIncome,
                Super = super
            };
        }

        private int CalculateGrossIncome(PayrollInput payrollInput)
        {
            return (int)Math.Round((decimal)payrollInput.AnnualSalary / 12, 0);
        }

        private int CalculateIncomeTax(PayrollInput payrollInput)
        {
            //Get the first matching tax record by salary
            TaxTable taxTable = _taxTableRepository.GetTaxTableBySalary(payrollInput.AnnualSalary);

            //Use the rate found above to calculate the income tax
            return (int)
                Math.Round(
                    (taxTable.BaseAmount
                    + (payrollInput.AnnualSalary - taxTable.FreeAmount)
                    * taxTable.TaxRate) / 12, 0);
        }
    }
}
