﻿using log4net;
using MYOB.Payroll.BOL;
using MYOB.Payroll.BOL.DAL;
using MYOB.Payroll.BOL.Input;
using MYOB.Payroll.BOL.Output;
using System;
using System.IO;
using System.Text;

namespace MYOB.Payroll.UI
{
    class Program
    {
        private static readonly ILog _log =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly TaxTableRepositoryCSV _taxTableRepository = new TaxTableRepositoryCSV();

        static void Main(string[] args)
        {
            while (true)
            {
                printMenu();

                string defaultInput = "AppData\\ValidData.csv";
                string defaultOutput = null;
                string command = Console.ReadLine();
                if ("EXIT".Equals(command, StringComparison.OrdinalIgnoreCase))
                    break;

                IPayrollInput payrollInput = new PayrollInputCSV();
                IPayrollOutput payrollOutput = null;
                switch (command)
                {
                    case "1":
                    case "5":
                        {
                            payrollOutput = new PayrollOutputCSV();
                            defaultOutput = "AppData\\Output.csv";
                            if (command == "5")
                                defaultInput = "AppData\\InvalidData.csv";

                            break;
                        }

                    case "2":
                        {
                            payrollOutput = new PayrollOutputXML();
                            defaultOutput = "AppData\\Output.xml";
                            break;
                        }

                    case "3":
                        {
                            payrollOutput = new PayrollOutputHTML();
                            defaultOutput = "AppData\\Output.html";
                            break;
                        }

                    case "4":
                        {
                            payrollOutput = new PayrollOutputJSON();
                            defaultOutput = "AppData\\Output.json";
                            break;
                        }

                    case "6":
                    case "7":
                        {
                            if (command == "6")
                                printTaxTable();
                            else if (command == "7")
                                printLogFile();

                            Console.WriteLine("Press any key to continue");
                            Console.ReadKey();
                            Console.Clear();

                            continue;
                        }
                }

                string inputFileName = GetInputFileName(defaultInput);
                string outputFileName = GetOutputFileName(defaultOutput);
                ProcessPayroll(payrollInput, payrollOutput, inputFileName, outputFileName);

                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
                Console.Clear();
            }
        }

        private static void ProcessPayroll(IPayrollInput payrollInput, IPayrollOutput payrollOutput, string inputFileName, string outputFileName)
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Input Class  : " + (payrollInput != null ? payrollInput.GetType().FullName : "Null"));
                Console.WriteLine($"Input Config : {inputFileName}");
                Console.WriteLine();

                Console.WriteLine("Output Class  : " + (payrollOutput != null ? payrollOutput.GetType().FullName : "Null"));
                Console.WriteLine($"Output Config: {outputFileName}");
                Console.WriteLine();

                Console.WriteLine("Results : ");

                PayrollProcessor payrollProcessor = new PayrollProcessor(_taxTableRepository, payrollInput, payrollOutput, _log);
                if (payrollProcessor.ProcessInput(inputFileName, outputFileName))
                    Console.WriteLine(File.ReadAllText(outputFileName));
            }
            catch (ApplicationException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static string GetOutputFileName(string defaultOutput)
        {
            Console.WriteLine($"Enter Output Filename? or Enter to use default ({defaultOutput})");
            string outputFileName = Console.ReadLine();
            outputFileName = string.IsNullOrWhiteSpace(outputFileName) ? defaultOutput : outputFileName;
            return outputFileName;
        }

        private static string GetInputFileName(string defaultInput)
        {
            Console.WriteLine($"Enter Input Filename? or Enter to use default ({defaultInput})");
            string inputFileName = Console.ReadLine();
            inputFileName = string.IsNullOrWhiteSpace(inputFileName) ? defaultInput : inputFileName;
            return inputFileName;
        }

        private static void printTaxTable()
        {
            Console.Clear();
            Console.WriteLine("Low       |High      |Base      |Free      |TaxRate   |");
            foreach (var data in _taxTableRepository.GetTaxTables())
                Console.WriteLine(
                    $"{data.LowAmount.ToString().PadLeft(10, ' ')}|" +
                    $"{data.HighAmount.ToString().PadLeft(10, ' ')}|" +
                    $"{data.BaseAmount.ToString().PadLeft(10, ' ')}|" +
                    $"{data.FreeAmount.ToString().PadLeft(10, ' ')}|" +
                    $"{data.TaxRate.ToString("0.####").PadLeft(10, ' ')}|");
        }

        private static void printLogFile()
        {
            Console.Clear();

            if (!File.Exists("MYOB.PayrollProcessor.log"))
                return;

            using (var reader =
                new StreamReader(
                    File.Open(
                        "MYOB.PayrollProcessor.log",
                        FileMode.Open,
                        FileAccess.Read,
                        FileShare.ReadWrite)))
            {
                Console.WriteLine(reader.ReadToEnd());
            }

        }

        private static void printMenu()
        {
            Console.WriteLine(CreateMYOBImage());
            Console.WriteLine("MYOB - Payroll Processor");
            Console.WriteLine("1) Run CSV Input with CSV Output");
            Console.WriteLine("2) Run CSV Input with XML Output");
            Console.WriteLine("3) Run CSV Input with HTML Output");
            Console.WriteLine("4) Run CSV Input with JSON Output");
            Console.WriteLine("5) Run CSV Input with invalid data");
            Console.WriteLine("6) Print current tax tables");
            Console.WriteLine("7) Print Logfile");
            Console.WriteLine();
            Console.WriteLine("Select output format (1, 2, 3, 4), Exit to quit");
        }

        /// <summary>
        /// Let's have some fun with the MYOB logo.....
        /// Print the MYOB logo as ASCII art.
        /// </summary>
        /// <returns></returns>
        private static string CreateMYOBImage()
        {
            StringBuilder data = new StringBuilder();
            data.AppendLine("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
            data.AppendLine("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNOxxKWMMMMMMMMMMMM");
            data.AppendLine("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNd,',xWMMMMMMMMMMMM");
            data.AppendLine("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMXl'',kWMMMMMMMMMMMM");
            data.AppendLine("MMMMWWXKKXNWMMMWNXKKXNWMMWNNNWWMMMMMMMMMWWNNNWMMWWXXXXNWMMMMWXl'',xWWNXXNNWMMMMM");
            data.AppendLine("MMNOo:,,,,;lk0kl:,,,,:lkXKo;::dXMMMMMMXdcc::xNNOdc:;;;:lx0NMWXl'',oxocc:cldOXWMM");
            data.AppendLine("W0c....','...'....,'....:Ox,...dNMMMMXl....:OOc'..,;:;,'',oKWXl',,,,,:cc;,,;ckNM");
            data.AppendLine("K:...:kKK0d,...'oOKKOc...:0d'..'kWMMNd'...;kk;..,o0XNXOl,''c0Xl',,,lOKXX0d:;;:xN");
            data.AppendLine("k,..,OMMMMNl...lXMMMWO;..,kXo...;0WWk,...'xKc..'dNMMMMWXl'''dKl',,oXWMMMMWk:;,c0");
            data.AppendLine("k'..;0MMMMNl...oNMMMM0;..'kWKc...cK0:..',oX0:..,kWMMMMMNd'''oKo'',xNMMMMMW0c;;cO");
            data.AppendLine("k'..;0MMMMNl...oNMMMM0;..'kWW0:...cc...lOXWNo..'lKWMMMWO:'',xKl',,cONWWWWXd;;;lK");
            data.AppendLine("k'..;0MMMMNl...oNMMMM0;..,kMMWO,......:0WWMMKc'.':okkxl;'',dXXl',',:oxkkdc;,;c0W");
            data.AppendLine("k'..lXMMMMNl..,kWMMMM0;..cKMMMWx'....;OWMMMMMXxc,...'''';lONWXl'';c:,,,,,,;cxKWM");
            data.AppendLine("XxxONMMMMMW0dxKWMMMMMNkdkXWMMMMXc...'xWMMMMMMMWN0kdoooxkKWMMMN0xx0XKOxdddxOXWMMM");
            data.AppendLine("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNx,..'dNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
            data.AppendLine("MMMMMMMMMMMMMMMMMMMMMMMMMMMNkl:'..'oXMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
            data.AppendLine("MMMMMMMMMMMMMMMMMMMMMMMMMMMWk,..,ckNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
            data.AppendLine("MMMMMMMMMMMMMMMMMMMMMMMMMMMMNkoxONWMMMMMMMMMMMMMMMMMMM     Love your Work     MM");
            return data.ToString();
        }
    }
}
