﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MYOB.Payroll.BOL;
using MYOB.Payroll.BOL.Input;
using MYOB.Payroll.BOL.Output;
using System.IO;
using MYOB.Payroll.BOL.Models;
using log4net;
using MYOB.Payroll.BOL.DAL;

namespace MYOB.Payroll.Test
{
    [TestClass]
    public class PayrollProcessorTest
    {
        private static readonly ILog _log =
              LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly TaxTableRepositoryCSV _taxRepository = new TaxTableRepositoryCSV();

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void Create_PayrollProcessor_WithNullInputs()
        {
            new PayrollProcessor(null, null, null, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void Process_With_InValid_Input()
        {
            var input = new PayrollInputCSV();
            var output = new PayrollOutputCSV();
            var processor = new PayrollProcessor(_taxRepository, input, output, _log);

            string inputFile = "AppData\\InvalidData.csv";
            string outputFile = "AppData\\Output" + ".csv";
            Assert.IsTrue(processor.ProcessInput(inputFile, outputFile), "Payroll Process not successful");
            Assert.IsTrue(File.Exists(outputFile), "Required output file not found");
        }

        [TestMethod]
        public void Process_Valid_Input_Valid_Output()
        {
            var input = new PayrollInputCSV();
            var output = new PayrollOutputCSV();
            var processor = new PayrollProcessor(_taxRepository, input, output, _log);
            var validInput = TestUtils.GetValidInputData().FirstOrDefault<PayrollInput>();
            var validOutput = TestUtils.GetValidOutputData().FirstOrDefault<PayrollOutput>();

            var result = processor.GetPayrollOutput(validInput);

            Assert.IsTrue(validOutput.Name.Equals(result.Name), "Name doesn't match");
            Assert.IsTrue(validOutput.PaymentPeriod.Equals(result.PaymentPeriod), "PaymentPeriod doesn't match");
            Assert.IsTrue(validOutput.GrossIncome.Equals(result.GrossIncome), "GrossIncome doesn't match");
            Assert.IsTrue(validOutput.IncomeTax.Equals(result.IncomeTax), "IncomeTax doesn't match");
            Assert.IsTrue(validOutput.NetIncome.Equals(result.NetIncome), "Net Income doesn't match");
            Assert.IsTrue(validOutput.Super.Equals(result.Super), "Super doesn't match");
        }

        [TestMethod]
        public void Process_With_Valid_Input_CSV()
        {
            var input = new PayrollInputCSV();
            var output = new PayrollOutputCSV();
            var processor = new PayrollProcessor(_taxRepository, input, output, _log);
            string inputFile = "AppData\\ValidData.csv";
            string outputFile = "AppData\\Output" + ".csv";
            Assert.IsTrue(processor.ProcessInput(inputFile, outputFile), "Payroll Process not successful");
            Assert.IsTrue(File.Exists(outputFile), "Required output file not found");
        }

        [TestMethod]
        public void Process_With_Valid_Input_XML()
        {
            var input = new PayrollInputCSV();
            var output = new PayrollOutputXML();
            var processor = new PayrollProcessor(_taxRepository, input, output, _log);
            string inputFile = "AppData\\ValidData.csv";
            string outputFile = "AppData\\Output" + ".xml";
            Assert.IsTrue(processor.ProcessInput(inputFile, outputFile), "Payroll Process not successful");
            Assert.IsTrue(File.Exists(outputFile), "Required output file not found");
        }


        [TestMethod]
        public void Process_With_Valid_Input_HTML()
        {
            var input = new PayrollInputCSV();
            var output = new PayrollOutputHTML();
            var processor = new PayrollProcessor(_taxRepository, input, output, _log);
            string inputFile = "AppData\\ValidData.csv";
            string outputFile = "AppData\\Output" + ".html";
            Assert.IsTrue(processor.ProcessInput(inputFile, outputFile), "Payroll Process not successful");
            Assert.IsTrue(File.Exists(outputFile), "Required output file not found");
        }


        [TestMethod]
        public void Process_With_Valid_Input_JSON()
        {
            var input = new PayrollInputCSV();
            var output = new PayrollOutputJSON();
            var processor = new PayrollProcessor(_taxRepository, input, output, _log);
            string inputFile = "AppData\\ValidData.csv";
            string outputFile = "AppData\\Output" + ".json";
            Assert.IsTrue(processor.ProcessInput(inputFile, outputFile), "Payroll Process not successful");
            Assert.IsTrue(File.Exists(outputFile), "Required output file not found");
        }
    }
}
