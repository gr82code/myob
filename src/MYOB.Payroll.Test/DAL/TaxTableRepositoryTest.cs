﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MYOB.Payroll.BOL.DAL;
using System.Collections.Generic;
using MYOB.Payroll.BOL.Models;

namespace MYOB.Payroll.Test.DAL
{
    [TestClass]
    public class TaxTableRepositoryTest
    {
        [TestMethod]
        public void Load_TaxTables_From_CSV()
        {
            var taxTableRepository = new TaxTableRepositoryCSV();
            IList<TaxTable> list = taxTableRepository.GetTaxTables().ToList();
            Assert.IsTrue(list.Count > 1);
        }

        [TestMethod]
        public void Get_One_TaxTable_From_Repository()
        {
            var taxTableRepository = new TaxTableRepositoryCSV();
            var taxTable = taxTableRepository.GetTaxTableBySalary(20000);

            Assert.IsTrue(taxTable.LowAmount.Equals(18201));
            Assert.IsTrue(taxTable.HighAmount.Equals(37000));
            Assert.IsTrue(taxTable.BaseAmount.Equals(0));
            Assert.IsTrue(taxTable.FreeAmount.Equals(18200));
            Assert.IsTrue(taxTable.TaxRate.Equals(0.19M));
        }


    }
}
