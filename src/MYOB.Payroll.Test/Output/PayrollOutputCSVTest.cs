﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using MYOB.Payroll.BOL.Input;
using MYOB.Payroll.BOL.Output;
using MYOB.Payroll.BOL.Models;
using System.Collections.Generic;

namespace MYOB.Payroll.Test
{
    [TestClass]
    public class PayrollOutputCSVTest :  PayrollOutputTest
    {
        protected override string GetFileExtension()
        {
            return ".csv";
        }

        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void Validation_Output_CSV_WithInvalid_FileName()
        {
            this.Generic_Validation_Output_WithInvalid_File(new PayrollOutputCSV());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Validation_Output_CSV_WithInvalid_Input()
        {
            this.Generic_Validation_Output_WithInvalid_Input(new PayrollOutputCSV());
        }

        [TestMethod]
        public void Process_Output_CSV_With_Valid_Data()
        {
            var output = new PayrollOutputCSV();
            bool result = this.Generic_Process_Output_With_Valid_Data(output);
            Assert.IsTrue(result);

            var data = File.ReadAllText("ValidOutput" + GetFileExtension());
            Assert.AreEqual(data
                .Replace("\r", string.Empty)
                .Replace("\n", string.Empty)
                .Replace("\t", string.Empty)
                .Replace(" ", string.Empty),
            TestUtils.GetValidOutputCSV()
                .Replace("\r", string.Empty)
                .Replace("\n", string.Empty)
                .Replace("\t", string.Empty)
                .Replace(" ", string.Empty));

        }
    }
}
