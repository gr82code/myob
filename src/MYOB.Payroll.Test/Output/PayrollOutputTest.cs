﻿using MYOB.Payroll.BOL;
using MYOB.Payroll.BOL.Input;
using MYOB.Payroll.BOL.Models;
using MYOB.Payroll.BOL.Output;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MYOB.Payroll.Test
{
    public abstract class PayrollOutputTest
    {
        protected abstract string GetFileExtension();

        protected void Generic_Validation_Output_WithInvalid_File(IPayrollOutput processor)
        {
            var data = TestUtils.GetValidOutputData();
            processor.ProcessOutput(data, @"c:\some:invalid:file:name" + GetFileExtension());
        }
        protected void Generic_Validation_Output_WithInvalid_Input(IPayrollOutput processor)
        {
            processor.ProcessOutput(null, @"DummyFile" + GetFileExtension());
        }
        protected bool Generic_Process_Output_With_Valid_Data(IPayrollOutput processor)
        {
            var data = TestUtils.GetValidOutputData();
            return processor.ProcessOutput(data, @"ValidOutput" + GetFileExtension());
        }
    }
}
