﻿using MYOB.Payroll.BOL;
using MYOB.Payroll.BOL.Input;
using MYOB.Payroll.BOL.Models;
using System.Collections.Generic;
using System.Linq;

namespace MYOB.Payroll.Test
{
    public abstract class PayrollInputTest
    {
        protected void Generic_Validation_Input_WithInvalid_File(IPayrollInput processor)
        {
            processor.ProcessInput(@"c:\some\file\that\doesn't exist.csv");
        }

        protected void Generic_Process_Input_With_Invalid_Data(IPayrollInput processor)
        {
            processor.ProcessInput("AppData\\InvalidData.csv");
        }

        protected IList<PayrollInput> Generic_Process_Input_With_Valid_Data(IPayrollInput processor)
        {
            return processor.ProcessInput("AppData\\ValidData.csv");
        }
    }
}
