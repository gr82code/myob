﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using MYOB.Payroll.BOL.Input;
using MYOB.Payroll.BOL.Models;

namespace MYOB.Payroll.Test
{
    [TestClass]
    public class PayrollInputCSVTest :  PayrollInputTest
    {
        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void Validation_Input_CSV_WithInvalid_File()
        {
            this.Generic_Validation_Input_WithInvalid_File(new PayrollInputCSV());
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void Process_Input_CSV_With_Invalid_Data()
        {
            this.Generic_Process_Input_With_Invalid_Data(new PayrollInputCSV());
        }

        [TestMethod]
        public void Process_Input_CSV_With_Valid_Data()
        {
            var input = new PayrollInputCSV();
            var validData = TestUtils.GetValidInputData().FirstOrDefault();
            var result = this.Generic_Process_Input_With_Valid_Data(input);

            Assert.IsTrue(result.Count == 2, "Does not contain 2 records");
            PayrollInput data = result.FirstOrDefault();
            Assert.IsTrue(validData.FirstName.Equals(data.FirstName), "FirstName doesn't match");
            Assert.IsTrue(validData.LastName.Equals(data.LastName), "LastName doesn't match");
            Assert.IsTrue(validData.AnnualSalary.Equals(data.AnnualSalary), "AnnualSalary doesn't match");
            Assert.IsTrue(validData.SuperRate.Equals(data.SuperRate), "SuperRate doesn't match");
            Assert.IsTrue(validData.PaymentPeriod.Equals(data.PaymentPeriod), "PayPeriod doesn't match");
        }
    }
}
