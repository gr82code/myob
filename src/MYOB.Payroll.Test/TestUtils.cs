﻿using MYOB.Payroll.BOL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MYOB.Payroll.Test
{
    internal static class TestUtils
    {
        static TestUtils() { }

        public static List<PayrollInput> GetValidInputData()
        {
            return new List<PayrollInput>() {
                new PayrollInput() {
                    FirstName = "David",
                    LastName = "Rudd",
                    AnnualSalary = 60050,
                    SuperRate = 0.09M,
                    PaymentPeriod = "01 March - 31 March"
                }
            };
        }

        public static List<PayrollOutput> GetValidOutputData()
        {
            return new List<PayrollOutput>() {
                new PayrollOutput() {
                    Name = "David Rudd",
                    GrossIncome = 5004,
                    IncomeTax = 922,
                    NetIncome = 4082,
                    PaymentPeriod = "01 March - 31 March",
                    Super = 450
                }
            };
        }

        public static string GetValidOutputCSV()
        {
            var data = GetValidOutputData().FirstOrDefault();
            return "name,pay period,gross income,income tax,net income,super\r\n" +
                   $"{data.Name},{data.PaymentPeriod},{data.GrossIncome},{data.IncomeTax},{data.NetIncome},{data.Super}\r\n";
        }
        public static string GetValidOutputJSON()
        {
            var data = GetValidOutputData().FirstOrDefault();
            return "[{" +
                $"\"Name\":\"{data.Name}\"," +
                $"\"PaymentPeriod\":\"{data.PaymentPeriod}\","+
                $"\"GrossIncome\":{data.GrossIncome},"+
                $"\"IncomeTax\":{data.IncomeTax},"+
                $"\"NetIncome\":{data.NetIncome},"+
                $"\"Super\":{data.Super}"+
                "}\r\n]";
        }
        public static string GetValidOutputXML()
        {
            var data = GetValidOutputData().FirstOrDefault();
            return
                "<?xml version=\"1.0\" encoding=\"utf - 8\"?>\r\n"+
                "<payroll>\r\n" +
                    "<employee>" +
                        $"<name>{data.Name}</name>" +
                        $"<payPeriod>{data.PaymentPeriod}</payPeriod>" +
                        $"<grossIncome>{data.GrossIncome}</grossIncome>" +
                        $"<incomeTax>{data.IncomeTax}</incomeTax>" +
                        $"<netIncome>{data.NetIncome}</netIncome>" +
                        $"<super>{data.Super}</super>" +
                    "</employee>\r\n" +
                "</payroll>";
        }
        public static string GetValidOutputHTML()
        {
            var data = GetValidOutputData().FirstOrDefault();
            return "<?xml version=\"1.0\" encoding=\"utf - 8\"?>\r\n"+
                "<table>\r\n" +
                    "<tr><th>name</th><th>payPeriod</th><th>grossIncome</th><th>incomeTax</th><th>netIncome</th><th>super</th></tr>\r\n" +
                    "<tr>" +
                        $"<td>{data.Name}</td>" +
                        $"<td>{data.PaymentPeriod}</td>" +
                        $"<td>{data.GrossIncome}</td>" +
                        $"<td>{data.IncomeTax}</td>" +
                        $"<td>{data.NetIncome}</td>" +
                        $"<td>{data.Super}</td>\r\n" +
                    "</tr>\r\n" +
                "</table>";
        }

    }
}
